  cd /opt/opencv/build
  sudo make -j4
  sudo make -j4 install
  sudo ldconfig

if [ "$ARDUINO" = true ]; then
  sudo apt-get install arduino arduino-core
fi

if [ "$SKYPE" = true ]; then
  sudo dpkg --add-architecture i386
  sudo apt-get update
  cd ~/Downloads
  sudo wget -O skype-install.deb http://www.skype.com/go/getskype-linux-deb
  sudo dpkg -i skype-install.deb
  sudo apt-get -f install
  #unpack version 4.2 saved in repo
fi

if [ "$STM32F4" = true ]; then
  cp 4* /etc/udev/rules.d/
  sudo service udev restart
  tar -xvjf gcc-arm-none-eabi-4_8-2014q2-20131204-linux.tar.bz2
  export PATH=$PATH:~/gcc-arm-none-eabi-4_8-2014q2/bin
  arm-none-eabi-gcc --version
  sudo apt-get install autoconf pkg-config libusb-1.0
  cd ~/Downloads
  git clone https://github.com/texane/stlink.git
  cd ~/Downloads/stlink
  ./autogen.sh
  ./configure
  make
  cd ~
  mkdir Development
  cd Development
  mkdir stm32 && cd stm32 && mkdir Projects && mkdir Documentation
  cd Documentation
  hg clone https://texasflood@bitbucket.org/texasflood/stm32f4library .
  cd ../Projects
  hg clone https://texasflood@bitbucket.org/texasflood/blinky
  hg clone https://texasflood@bitbucket.org/texasflood/processor
fi

#Rubyripper
#Dependencies
#sudo apt-get install cd-discid cdparanoia flac lame mp3gain normalize-audio ruby-gnome2 ruby vorbisgain
#sudo apt-get install libgettext-ruby1.8 make
#cd /usr/local/src
#ls -ld .
#sudo chgrp $USER .
#sudo chmod g+w .
#wget http://rubyripper.googlecode.com/files/rubyripper-0.6.2.tar.bz2
#sha1sum rubyripper-0.6.2.tar.bz2
#tar xvjf rubyripper-0.6.2.tar.bz2
#cd rubyripper-0.6.2
#./configure --enable-lang-all --enable-gtk2 --enable-cli --prefix=/usr/local
#sudo make install

#Project 
echo "Installing 4th year project stuff now"
sudo apt-get install gfortran liblapack-dev libatlas-base-dev libatlas-dev
git clone git://github.com/xianyi/OpenBLAS ~/Downloads/OpenBLAS
sudo mv ~/Downloads/OpenBLAS /opt
cd /opt/OpenBLAS
sudo make
udo make install
wget http://sourceforge.net/projects/arma/files/armadillo-4.450.3.tar.gz -P ~/Downloads
cd ~/Downloads
tar xvfz armadillo-4.450.3.tar.gz
sudo mv armadillo-4.450.3 /opt
cd /opt/armadillo*
sudo cmake .
sudo make
sudo make install
cd /opt/OpenBLAS
sudo cp libopenblas* /usr/lib
cmake .

sudo apt-get install freeglut3-dev libxi-dev libxmu-dev
cd ~/Downloads
git clone git://github.com/simbody/simbody simbody-source
sudo mv simbody-source /opt
cd /opt/simbody-source
git checkout Simbody-3.5.1
sudo mkdir /opt/simbody-build-rel
cd /opt/simbody-build-rel
sudo cmake /opt/simbody-source -DCMAKE_INSTALL_PREFIX=/opt/simbody-build-rel -DCMAKE_BUILD_TYPE=Release
sudo make -j4
sudo ctest -j4
sudo make -j4 install

sudo mkdir /opt/simbody-build-deb
cd /opt/simbody-build-deb
sudo cmake /opt/simbody-source -DCMAKE_INSTALL_PREFIX=/opt/simbody-build-deb -DCMAKE_BUILD_TYPE=Debug
sudo make -j4
sudo ctest -j4
sudo make -j4 install

#echo 'LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/simbody-build-rel/lib/x86_64-linux-gnu' >> ~/.bashrc
#echo 'LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/simbody-build-deb/lib/x86_64-linux-gnu' >> ~/.bashrc
#echo 'export LD_LIBRARY_PATH' >> ~/.bashrc
#sudo update-alternatives --set liblapack.so.3gf /usr/lib/lapack/liblapack.so.3gf #Don't know if this is needed?

#Vim
cd ~/Downloads
hg clone https://vim.googlecode.com/hg/ vim
sudo mv vim /opt
cd /opt/vim/src
./configure \
  --enable-perlinterp \
  --enable-pythoninterp \
  --enable-rubyinterp \
  --enable-cscope \
  --enable-gui=auto \
  --enable-gtk2-check \
  --enable-gnome-check \
  --with-features=huge \
  --enable-multibyte \
  --with-x \
  --with-compiledby="Anas" \
  --with-python-config-dir=/usr/lib/python2.7/config-x86_64-linux-gnu #ubuntu
#  --with-python-config-dir=/usr/lib/python2.7/config #debian wheezy
sudo make -j4
sudo make -j4 install
sudo update-alternatives --install "/usr/bin/vim" "vim" "/usr/local/bin/vim" 1
sudo easy_install psutil #For atp_tex

#vim terminal
echo "set editing-mode vi" >> ~/.inputrc
echo "set keymap vi-command" >> ~/.inputrc

#Extra stuff
#Replace chrome logo
#sudo cp /usr/local/share/icons/hicolor/256x256/apps/google-chrome.png /usr/local/share/icons/hicolor/48x48/apps

#copy ubuntu fonts from packages, replace etc/shared/fonts and find ubuntu font packages and replace 
#http://noz3001.wordpress.com/2011/07/01/ubuntu-font-rendering-on-debian-wheezy/
#http://www.webupd8.org/2013/06/better-font-rendering-in-linux-with.html
#echo "deb http://ppa.launchpad.net/no1wantdthisname/ppa/ubuntu precise main" | sudo tee /etc/apt/sources.list.d/infinality.list
#sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E985B27B
#sudo apt-get update
#sudo apt-get install fontconfig-infinality

#Add special config for touchpad gestures and controls in X11/xorg*/synaptics.conf
#Set vim as default editor over gedit
#some things in this script file assume debian and not ubuntu
# in gnome tweak tool turn off dynamic workspaces and set full path to always show
#in banshee, go to preferences and enable metadata and file syncing
# replace line in ~/.gnome2/accels/nautilus (gtk_accel_path "<Actions>/DirViewActions/Trash" "Delete") 

#Add Gnome extensions
#Activities Configurator
#Advanced Settings in User menu
#Advanced Volume mixer
#Alternative status menu
#Battery remaining time and percentage
#CPU Freq
#Impatience
#Remove accessibility
#Antisocial
#Sensors
#SettingsCenter
#Workspace Grid

#Get alsa: http://howto.blbosti.com/2010/04/ubuntu-make-alsa-default-instead-of-pulseaudio/
#sudo apt-get install python-notify python-gtk2 python-alsaaudio python-eggtrayicon xfce4-mixer
cd ~/Downloads
#wget http://howto.blbosti.com/files/alsa/alsa_mixer_applet_1.1.tar.gz
#sudo tar -C /usr/local/bin/ -xzvf alsa_mixer_applet_1.1.tar.gz
#cd /usr/local/bin
#sudo chmod +x alsa*
#sudo chmod +x volbar.py

#Add the following shortcuts:
#Name:      ALSA Volume mute
#Command:   /usr/local/bin/alsa_master_mute
#Name:      ALSA Volume down
#Command:   /usr/local/bin/alsa_master_down
#Name:      ALSA Volume up
#Command:   /usr/local/bin/alsa_master_up

#Add to startup:
#Name:      ALSA Volume mute
#Command:   /usr/local/bin/alsa_master_mute
#Name:      ALSA Volume down
#Command:   /usr/local/bin/alsa_master_down
#Name:      ALSA Volume up
#Command:   /usr/local/bin/alsa_master_up

#Remove from startup:
#Pulseaudio
#caribou
#Orca
#Evolution alarm notify
#Desktop sharing
#Bluetooth manager

#sed -i 's/; (gtk_accel_path "<Actions>\/DirViewActions\/Trash" "<Primary>Delete")/(gtk_accel_path "<Actions>\/DirViewActions\/Trash" "Delete")/' ~/.gnome2/accels/nautilus

#sed 's/https:\/\/texasflood@bitbucket.org\/texasflood\/javapuzzlers/https:\/\/texasflood@bitbucket.org\/texasflood\/hello/' <hgrc >new
#Loop over all repos in bitbucket and clone them, use the sed command to update the hgrc

cd ~
echo "Done"
