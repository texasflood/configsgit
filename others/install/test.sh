#! /bin/bash

for topLevel in ~/configs/ ~/configs/private/
do
  for dirType in home/ root/
  do
    cd "$topLevel""$dirType"
    if [ "$?" != 0 ]; then
      continue
    fi
    for config in $(ls -A)
    do
      if [ "$dirType" = home/ ]; then
        echo "$topLevel""$dirType""$config"
        ln -sf "$topLevel""$dirType""$config" ~/"$config" 
      elif [ "$dirType" = root/ ]; then
        for recursedFile in $(find "$config" -type f)
        do
          echo $(pwd)"/""${recursedFile}"
          sudo ln -sf $(pwd)"/""${recursedFile}" "/""${recursedFile}" # #? removes first char 
        done
      else
        echo "Error"
        return 1
      fi
    done
  done
done
echo "Done"
