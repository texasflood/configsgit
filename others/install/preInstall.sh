#! /bin/bash

sudo apt-get install mercurial
cd
hg clone https://texasflood@bitbucket.org/texasflood/configs
cd configs/
hg clone https://texasflood@bitbucket.org/texasflood/privateConfigs private
cd others/install/
./install.sh
