#!/bin/bash

source="/home/anas/Pictures/Unorganised"
target="/home/anas/Pictures/Organisedtest"

find "$source" -type f -iname "*" | while read file ; do
  date=$(date +"%Y:%m:%d" -r "$file")
  echo "$date"
  [ -z "$date" ] && echo "$file" >> ~/error.txt && continue
  year=${date%%:*}
  month=${date%:*}
  month=${month#*:}
  day=${date##*:}
  mkdir -p "${target}/${year}"
  mkdir -p "${target}/${year}/${month}-${day}-${year}"
  mv "$file" "${target}/${year}/${month}-${day}-${year}"
done
