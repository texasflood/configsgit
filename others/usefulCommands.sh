#! /bin/bash

if [ "0" = "0" ]; then
  sudo dmidecode -t system # system info
  unicode 'a acute'
  du -had 1 . | sort -h # Sorted filesizes
  vim ~/.local/share/applications/mimeapps.list # edit defaults
  sudo pkill -USR1 -n -x dd # Check dd status
fi

