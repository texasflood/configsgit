#!/usr/bin/env python

#--------------------------------------------
# alsavol.py
# Version 0.2
# author: richard_jonsson at hotmail.com
#
# Keyboard volume key control for alsa.
# Listens on a fifo for commands 'up, 'down' and 'mute'
# and displays the action in a OSD bubble.
#--------------------------------------------

import alsaaudio
import pygtk
pygtk.require('2.0')
import pynotify
import sys
import os

mixer_fifo = '/tmp/alsa_mixer_command_queue';
volume_step = 4

def ShowOSD(osd):
  m = alsaaudio.Mixer('Master')
  level = m.getvolume()[0]
  muted = m.getmute()[0]
  msg = 'Master volume: '+str(level)+'%'
  if muted:
    msg += ', muted'
    icon = 'notification-audio-volume-muted'
  else:
    icon = 'notification-audio-volume-'
    if level < 1:
      icon += 'off'
    elif level < 33:
      icon += 'low'
    elif level < 67:
      icon += 'medium'
    else:
      icon += 'high'
  print 'icon: '+icon
  osd.update(msg, '', icon)

  osd.show()

def VolDown(osd):
  global volume_step
  print 'Volume down'
  m = alsaaudio.Mixer('Master')
  level = m.getvolume()[0]-volume_step
  if level < 0:
    level = 0
  m.setvolume(level)
  ShowOSD(osd)

def VolUp(osd):
  global volume_step
  print 'Volume up'
  m = alsaaudio.Mixer('Master')
  level = m.getvolume()[0]+volume_step
  if level > 100:
    level = 100
  m.setvolume(level)
  ShowOSD(osd)

def VolMute(osd):
  print 'Master mute'
  m = alsaaudio.Mixer('Master')
  muted = m.getmute()[0]
  if muted:
    m.setmute(0)
  else:
    m.setmute(1)

  ShowOSD(osd)

def Dispatch(osd,action):
  action = action.strip()
  if action == 'down':
    VolDown(osd)

  elif action == 'up':
    VolUp(osd)

  elif action == 'mute':
    VolMute(osd)

pynotify.init("Test")
osd = pynotify.Notification("Notify test", "testing osd blabla", "Play")

try:
  os.remove(mixer_fifo)
except:
  print 'bleh'
os.mkfifo(mixer_fifo)

while 1:
  f=open(mixer_fifo, 'r')
  ln = f.readline()
  f.close()
  Dispatch(osd, ln)

