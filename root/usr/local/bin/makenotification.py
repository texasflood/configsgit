#!/usr/bin/env python
import pynotify
import os
import traceback

mixer_fifo = '/tmp/makenotification';

pynotify.init("!")
osd = pynotify.Notification("", "", "")

try:
  os.remove(mixer_fifo)
except:
  pass
os.mkfifo(mixer_fifo)

while 1:
  try:
    f=open(mixer_fifo, 'r')
    ln = f.readline()
    f.close()
    osd.update (ln, '', '')
    osd.show()
  except Exception as e:
    exc_type, exc_value, exc_traceback = sys.exc_info()
    traceback.print_exception(exc_type, exc_value, exc_traceback,
                              limit=4, file=sys.stdout)
    runtimeError('There has been an error within the block of type '
     + ''', please check your code!
     The exception type is
     '''
     + str(type(e)) + '''
     and the message is
     ''' + str(e))
    print "HERE"
    exc = str(traceback.format_exec())
    os.system ('echo ' + exc + ' >> ~/.makeNotError.txt')

