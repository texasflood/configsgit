#!/usr/bin/env python

#--------------------------------------------
# alsavol.py
# Version 0.2
# author: richard_jonsson at hotmail.com
#
# Keyboard volume key control for alsa.
# Listens on a fifo for commands 'up, 'down' and 'mute'
#--------------------------------------------

import alsaaudio
import pygtk
pygtk.require('2.0')
import pynotify
import sys
import os

mixer_fifo = '/tmp/alsa_mixer_command_queue';
volume_step = 4

def VolDown():
  global volume_step
  m = alsaaudio.Mixer('Master')
  muted = m.getmute()[0]
  if muted:
    m.setmute(0)
  level = m.getvolume()[0]-volume_step
  if level < 0:
    level = 0
  m.setvolume(level)

def VolUp():
  global volume_step
  m = alsaaudio.Mixer('Master')
  muted = m.getmute()[0]
  if muted:
    m.setmute(0)
  level = m.getvolume()[0]+volume_step
  if level > 100:
    level = 100
  m.setvolume(level)

def VolMute():
  m = alsaaudio.Mixer('Master')
  muted = m.getmute()[0]
  if muted:
    m.setmute(0)
  else:
    m.setmute(1)


def Dispatch(action):
  action = action.strip()
  if action == 'down':
    VolDown()

  elif action == 'up':
    VolUp()

  elif action == 'mute':
    VolMute()

try:
  os.remove(mixer_fifo)
except:
  pass
os.mkfifo(mixer_fifo)

while 1:
  f=open(mixer_fifo, 'r')
  ln = f.readline()
  f.close()
  Dispatch(ln)
