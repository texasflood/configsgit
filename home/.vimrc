filetype plugin on "Causes python indentation to go wrong
filetype indent on
set expandtab
set gdefault
set shiftwidth=2
set softtabstop=2
set smartindent
set incsearch
set hlsearch
set backspace=2
set mouse=a
set mousemodel=popup_setpos
set ignorecase
set smartcase
set ruler
set relativenumber
set number
set nocompatible
set magic
set display+=lastline
syntax on
"let g:ring = 1

if (&term =~ "xterm.*")
  set t_Co=256
  colorscheme badwolf
else
  let g:CSApprox_loaded=1
  colorscheme delek
endif

let mapleader = "-"
filetype off
call pathogen#infect() 

filetype plugin on "Causes python indentation to go wrong
filetype indent on

set omnifunc=syntaxcomplete#Complete

nnoremap <silent> <leader><space> :nohlsearch <bar> call clearmatches() <cr>
"nnoremap <leader><space> :nohlsearch | call matchdelete(0)<cr>
inoremap <C-B> <C-O>yiW<End>=<C-R>=<C-R>0<CR>

autocmd FileType cpp source ~/.vim/c.vim
autocmd FileType c source ~/.vim/c.vim
autocmd FileType hpp source ~/.vim/c.vim
autocmd FileType h source ~/.vim/c.vim

autocmd FileType tex source ~/.vim/tex.vim
autocmd FileType python source ~/.vim/py.vim
autocmd BufNewFile,BufRead *.txt source ~/.vim/txt.vim

nnoremap <leader>= gg=G``
nnoremap <leader>s :call ToggleScrollbar()<CR>
"nnoremap <leader>b hv?\S
nnoremap <tab> %
vnoremap <tab> %
nnoremap <S-F8> :GundoToggle<CR>
nnoremap ; :
vnoremap ; :
nnoremap : ;
vnoremap : ;
nnoremap <CR> ZZ
nnoremap Y y$
nnoremap <S-Left> gT
nnoremap <S-Right> gt
nnoremap <S-Up> 1gt
nnoremap <S-Down> :tablast<CR>
inoremap <F1>  <ESC>l1gti
inoremap <F2>  <ESC>l2gti
inoremap <F3>  <ESC>l3gti
inoremap <F4>  <ESC>l4gti
inoremap <F5>  <ESC>l5gti
inoremap <F6>  <ESC>l6gti
inoremap <F7>  <ESC>l7gti
inoremap <F8>  <ESC>l8gti
inoremap <F9>  <ESC>l9gti
inoremap <F10> <ESC>l10gti
vnoremap <F1>  1gt
vnoremap <F2>  2gt
vnoremap <F3>  3gt
vnoremap <F4>  4gt
vnoremap <F5>  5gt
vnoremap <F6>  6gt
vnoremap <F7>  7gt
vnoremap <F8>  8gt
vnoremap <F9>  9gt
vnoremap <F10> 10gt
nnoremap <F1>  1gt
nnoremap <F2>  2gt
nnoremap <F3>  3gt
nnoremap <F4>  4gt
nnoremap <F5>  5gt
nnoremap <F6>  6gt
nnoremap <F7>  7gt
nnoremap <F8>  8gt
nnoremap <F9>  9gt
nnoremap <F10> 10gt
"cw does not do what I expect or want!
nnoremap cw dwi
"Special delete function to take one letter words into account
nnoremap <leader>de vhed
"Special clear function to take one letter words into account
nnoremap <leader>ce vhec

nnoremap <space> :update<CR>
"au FocusLost * :wa "Doesn't work
let &t_SI .= "\<Esc>[?2004h"
let &t_EI .= "\<Esc>[?2004l"

inoremap <special> <expr> <Esc>[200~ XTermPasteBegin()
cmap w!! w !sudo tee > /dev/null %

"Save folds
autocmd BufWinLeave *.* mkview
"Causes python indentation to go wrong
autocmd BufWinEnter *.* silent loadview
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>

augroup my_project
  autocmd BufWritePost ~/*obotics/*.cpp let x = system('(make -C build/ ;  if [ "$?" = "0" ]; then notify-send -i emblem-default --hint=int:transient:1 "Make complete"; else notify-send -i error --hint=int:transient:1 "Make error"; fi )&')
augroup END

function! HLNext (blinktime)
  "call matchdelete(g:ring)
  highlight WhiteOnRed ctermfg=white ctermbg=red
  let [bufnum, lnum, col, off] = getpos('.')
  "Doesn't work!
  "if (&ignorecase) 
    "let target_pat = '\c\%#\%('.@/.'\)'
  "else 
    "let target_pat = '\C\%#\%('.@/.'\)'
  "end
  let target_pat = '\c\%#\%('.@/.'\)'
  let g:ring = matchadd('WhiteOnRed', target_pat, 101)
  "echo g:ring
  redraw
endfunction
autocmd VimLeave * call system("xsel -ib", getreg()) "Saves +buffer
nnoremap <silent> n   n:call HLNext(0.1)<cr>
nnoremap <silent> N   N:call HLNext(0.1)<cr>

command! -nargs=+ Calp :call Calculator(<q-args>) | normal! p
py from math import *
py from cmath import *

fun Calculator(arg)
  redir @"
  execute "py print " a:arg
  redir END
  let @" = strpart(@", 1)
endfun

sign define scrollbox texthl=Visual text=[]
fun! ScrollbarGrab()
  if getchar()=="\<leftrelease>" || v:mouse_col!=1
    return|en
  while getchar()!="\<leftrelease>"
    let pos=1+(v:mouse_lnum-line('w0'))*line('$')/winheight(0)
    call cursor(pos,1)
    sign unplace 789
    exe "sign place 789 line=".(pos*winheight(0)/line('$')+line('w0')).b:scrollexpr
  endwhile
endfun
fun! UpdateScrollbox()
  sign unplace 789
  exe "sign place 789 line=".(line('w0')*winheight(0)/line('$')+line('w0')).b:scrollexpr
endfun

fun! ToggleScrollbar()
  if exists('b:opt_scrollbar')
    unlet b:opt_scrollbar
    nun <buffer> <leftmouse>
    iun <buffer> <leftmouse>
    nun <buffer> <scrollwheelup>
    nun <buffer> <scrollwheeldown>
    iun <buffer> <scrollwheelup>
    iun <buffer> <scrollwheeldown>
    exe "sign unplace 789 file=" . expand("%:p")
    exe "sign unplace 788 file=" . expand("%:p")
  else
    let b:opt_scrollbar=1
    nno <silent> <buffer> <leftmouse> <leftmouse>:call ScrollbarGrab()<cr>
    ino <silent> <buffer> <leftmouse> <leftmouse><c-o>:call ScrollbarGrab()<cr>
    nno <buffer> <scrollwheelup> <scrollwheelup>:call UpdateScrollbox()<cr>
    nno <buffer> <scrollwheeldown> <scrollwheeldown>:call UpdateScrollbox()<cr>
    ino <buffer> <scrollwheelup> <scrollwheelup><c-o>:call UpdateScrollbox()<cr>
    ino <buffer> <scrollwheeldown> <scrollwheeldown><c-o>: call UpdateScrollbox()<cr>
    let b:scrollexpr=" name=scrollbox file=".expand("%:p")
    exe "sign place 789 line=".(line('w0')*winheight(0)/line('$')+line('w0')).b:scrollexpr
    exe "sign place 788 line=1".b:scrollexpr
  end
endfun
function! XTermPasteBegin()
  set pastetoggle=<Esc>[201~
  set paste
  return ""
endfunction

function! CopyMatches(reg)
  let hits = []
  %s//\=len(add(hits, submatch(0))) ? submatch(0) : ''/ge
  let reg = empty(a:reg) ? '+' : a:reg
  execute 'let @'.reg.' = join(hits, "\n") . "\n"'
endfunction
command! -register CopyMatches call CopyMatches(<q-reg>)

"/\(All time fittest:$\n[\)\@<=\d*\.\d*

