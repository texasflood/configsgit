setlocal spell spelllang=en_gb
setlocal shiftwidth=2
setlocal tabstop=2
setlocal nosmartindent
setlocal linebreak
nnoremap <F2> :clist<CR>
nnoremap <F3> :cprev<CR>
nnoremap <F4> :cnext<CR>
inoremap -VVV \vspace{\baselineskip}

set grepprg=grep\ -nH\ $*
let g:tex_flavor='latex'
let g:Tex_DefaultTargetFormat='pdf'
let g:Tex_MultipleCompileFormats='pdf,dvi'
let g:Tex_ViewRule_pdf='open'

nnoremap <silent> \l :let x = system('(pdflatex -interaction=nonstopmode ' . @% . ' > texErrors )& ')<CR>
command View :let x = system('xdg-open ' . substitute(@%, ".tex\$", ".pdf", "") . ' &> /dev/null 2>&1 ')
command ShowErrors :call TexErrors()
set errorformat=%f:%l:%c:%m 
function TexErrors() 
    update
    let errors=system('cat texErrors | latex-errorfilter') 
    if errors=="" 
        echo 'LaTeX ok: No warning/error' 
    else 
        cexpr errors 
    endif 
endfunction
augroup texMake
  autocmd BufWritePost * :let x = system('(pdflatex -interaction=nonstopmode ' . @% . ' > texErrors )& ')
augroup END
